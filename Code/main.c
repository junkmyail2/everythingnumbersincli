#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
int pick;
long double num1modified;
long double num2modified;
int main() {

printf("Welcome to the EverythingNumbersInsideCLI!\n");

printf("Please select your choice of operation to perform.\n");

printf("1. Calculator (BETA) (key: 1)\n");
printf("2. AveraMedian (BETA) (key: 2)\n");

 scanf("%i", &pick);

if (pick == 1) {
    long double num1;
    long double num2;
    long double result;
    int operation;

    printf("Welcome to CCalc, a terminal calculator written in C.\n");

    printf("Before we begin, you may enter modifiers to the 1st and second numbers, e.g a percentage if you wish. Currently, we are going to focus on the first number. If you would like to add a modifier to it, press and return y. If not, press n to continue without using any modifiers.");
    printf("The equation format is (num1 *operation* num2.)\n");
    printf("Would you like to apply a modifier to the first number (num1)? (y/n)\n");

    char optionmodifierchoice1;
    scanf(" %c", &optionmodifierchoice1);
    bool modifierpickedswitch1;

    if (optionmodifierchoice1 == 'y') {
        modifierpickedswitch1 = true;
        printf("You have chosen to apply a modifier to your number. Refer to the list below to select your modifier.\n");
        printf("1. Percent (key: per)\n");
        printf("2. Square Root (key: sqrroot)\n");
        char modifier1[10];
        scanf(" %s", modifier1);
            if (strcmp(modifier1, "per") == 0) {
                printf("You have chosen to make your first number a percentage. What value will this percent have?\n");
                long double pctval;
                scanf("%Lf", &pctval);
                num1modified = pctval / 100;
            }
            if (strcmp(modifier1, "sqrroot") == 0) {
                printf("You have chosen to make your first number a square root. What value will this square root have?\n");
                long double sqrroot;
                scanf("%Lf", &sqrroot);
                num1modified = sqrt(sqrroot);
            }
            printf("Success! Your first number is now modified. If you would also like to apply a modifier to your second number, please press y, otherwise, press n to decline using a modifier to your second number.\n");
}

    if (optionmodifierchoice1 == 'n') {
        printf("You have chosen to not pick a modifier for the first number and use the regular format. Please input your number.\n");
}

    if (modifierpickedswitch1 == false) {
        scanf("%Lf", &num1);
        printf("Chosen number for num1 was %Lf\n", num1);
        printf("Great! You may carry on to the next number. For this number, you may also add a modifier if you wish. If you do, press and return y to select a modifier for it, otherwise, press n.");
}
    else {
        num1 = num1modified;
}

    char optionmodifierchoice2;
    scanf(" %s", &optionmodifierchoice2);
    bool modifierpickedswitch2;

    if (optionmodifierchoice2 == 'y') {
        modifierpickedswitch2 = true;
        printf("You have chosen to apply a modifier to your second number. Please look into the list below to view your choices.\n");
        printf("1. Percent (key: per)\n");
        printf("2. Square Root (key: sqrroot)\n");
        char modifier2[10];
        scanf(" %s", modifier2);
            if (strcmp(modifier2, "per") == 0) {
                printf("You have chosen to apply a percentage. What value shall this percent have?\n");
                long double pctval;
                scanf("%Lf", &pctval);
                num2modified = pctval / 100;
            }
            if (strcmp(modifier2, "sqrroot") == 0) {
                long double sqrroot;
                printf("You have chosen to make your number a square root. What value shall this square root have?\n");
                scanf("%Lf", &sqrroot);
                num2modified = sqrt(sqrroot);
            }
    }
    if (optionmodifierchoice2 == 'n') {
        modifierpickedswitch2 = false;
        printf("You have picked to not apply a modifier to your second number. Please input your second number below.\n");
    }

if (modifierpickedswitch2 == false) {
    scanf("%Lf", &num2);
    printf("Chosen number for num2 was %Lf\n", num2);
}
else {
    num2 = num2modified;
}
    printf("Now, please select an operation, 1 for addition, 2 for subtraction, 3 for division, and 4 for multiplication.\n");

    scanf("%i", &operation);

    if (operation == 1) {
        result = num1+num2;
    }

    if (operation == 4) {
        result = num1*num2;
    }

    if (operation == 3) {
        result = num1/num2;
    }

    if (operation == 2) {
    result = num1-num2;
    }

    printf("Your result is %Lf. Thank you for using CCalc!\n", result);
exit(0);
}


if (pick == 2) {
    printf("Welcome to AveraMedian. Please select whether to find a average (1) or a median (NOT RELEASED YET)\n");

    int pickAV;
    scanf("%i", &pickAV);
    if (pickAV == 1) {
        printf("Looks like you have chosen to find an average. Please proceed with your calculation below.\n");
        
    long double number, sum = 0, mean = 0, unusedNum;
    int numAmtAV = 0;
    bool contAV = true;

    printf("Before we begin finding your average, we need to find out a number (should be a one that will NOT be used in your calculation) in order for the loop to be able to quit and the calculation of your mean to work.\n");
    printf("Please input your magical loop-quitting number below. Don't forget to make sure it's a number you won't be using in your calculation!:\n");
    scanf("%Lf", &unusedNum);
    printf("Great! The program will now proceed to the loop. Don't forget to type in that number when you want to quit the loop and calculate your average!\n");
    while (contAV == true) {
        printf("Please input a number:\n");
        scanf("%Lf", &number);
        printf("number chosen: %Lf\n", number);
        if (number == unusedNum) {
            contAV = false;
        }    
        else {
            sum = number + sum;
            numAmtAV = numAmtAV + 1;
            } 
    }
    mean = sum / numAmtAV;
    printf("Total value of sum: %Lf\n", sum);
    printf("Total amount of times the loop ran: %d\n", numAmtAV);
    printf("Your mean is %Lf\n", mean);
    }
    if (pickAV == 2) {
        /* will work on later */
    }
}

if (pick == 3) {
    printf("");
}
}
